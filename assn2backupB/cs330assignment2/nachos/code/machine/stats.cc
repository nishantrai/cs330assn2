// stats.h 
//	Routines for managing statistics about Nachos performance.
//
// DO NOT CHANGE -- these stats are maintained by the machine emulation.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "utility.h"
#include "stats.h"

//----------------------------------------------------------------------
// Statistics::Statistics
// 	Initialize performance metrics to zero, at system startup.
//----------------------------------------------------------------------

Statistics::Statistics()
{
    totalTicks = idleTicks = systemTicks = userTicks = 0;
    numDiskReads = numDiskWrites = 0;
    numConsoleCharsRead = numConsoleCharsWritten = 0;
    numPageFaults = numPacketsSent = numPacketsRecvd = 0;
    totalBurst = currentStart = globalStart = totalTime = numBursts = totalWait = 0;
    expTimeSquare = expTime = totThreads = maxCompletionTime = varCompletionTime = 0;
    minCompletionTime = 10000000;
    minBurst = 10000000;
    maxBurst = 0;
    estimateError = 0;
}

//----------------------------------------------------------------------
// Statistics::Print
// 	Print performance metrics, when we've finished everything
//	at system shutdown.
//----------------------------------------------------------------------

void
Statistics::Print()
{
    printf("Ticks: total %d, idle %d, system %d, user %d\n", totalTicks, 
	idleTicks, systemTicks, userTicks);
    
    totalTime = totalTicks - globalStart;

    expTimeSquare /= totThreads;
    expTime /= totThreads;
    
    varCompletionTime = (expTimeSquare - (expTime*expTime));

    printf("The total ticks %d, Current ticks is %d\n",totalBurst,currentStart);
    printf("The total time %d\n",totalTime);
    printf("The max CPU burst : %d, min CPU burst : %d\n",maxBurst,minBurst);

    printf("The number of CPU bursts are %d\n", numBursts);
    printf("The total wait time is %d\n", totalWait);
    printf("Average Wait time : %f\n", ((totalWait*(1.0))/(totThreads+1)));

    printf("The Average, Variance, Max, Min Completion time : %f, %f, %f, %f\nThe total threads are : %f\n",
     expTime, varCompletionTime, maxCompletionTime, minCompletionTime, totThreads);

    printf("Average CPU burst time : %f\n",((totalBurst*(1.0))/numBursts));
    printf("CPU utilisation : %f\n",((totalBurst*(1.0))/totalTime));

    printf("The total estimation error : %f\n",estimateError);
    printf("The average estimation error is : %f\n",((estimateError*(1.0))/totalBurst));

    printf("Disk I/O: reads %d, writes %d\n", numDiskReads, numDiskWrites);
    printf("Console I/O: reads %d, writes %d\n", numConsoleCharsRead, 
	numConsoleCharsWritten);
    printf("Paging: faults %d\n", numPageFaults);
    printf("Network I/O: packets received %d, sent %d\n", numPacketsRecvd, 
	numPacketsSent);
}
